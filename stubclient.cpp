#define SIZE 512

#include <iostream>
#include <cstdlib>
#include "gen/stubclient.h"
#include <jsonrpccpp/client/connectors/httpclient.h>
#include "parser/include/parser.h"

using namespace jsonrpc;
using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        cout << "usage: ./client <port>" << endl;
        return 1;
    }
    string address = "http://localhost:";
    address += argv[1];
    HttpClient httpclient(address);
    StubClient c(httpclient, JSONRPC_CLIENT_V2);

    while (true) {
        try {
            char *request_str = (char *) malloc(sizeof(char) * SIZE);
            puts("Enter request > ");
            fgets(request_str, SIZE, stdin);
            size_t len = strlen(request_str);
            struct request *r = parse_request(request_str, len - 1);

            if (r == nullptr) {
                cout << "invalid request\n";
            } else {
                if (r->operation == '+') {
                    Json::Value paramsList(Json::arrayValue);
                    paramsList.append(r->tuple_list->next->attribute_list->field);
                    paramsList.append(r->tuple_list->next->attribute_list->value);

                    struct attribute* a = r->tuple_list->next->attribute_list->composite_attribute;
                    while (a != nullptr) {
                        paramsList.append(a->field);
                        paramsList.append(a->value);
                        a = a->composite_attribute;
                    }

                    cout << c.add(paramsList, r->tuple_list->name);
                    free(request_str);
                } else if (r->operation == '-') {
                    cout << c.remove(r->tuple_list->name);
                } else if (r->operation == '?') {
                    if (r->tuple_list->attribute_list == nullptr && ::strcmp(r->tuple_list->name, "*") == 0) {
                        cout << c.print();
                    } else if (r->tuple_list->name != nullptr && r->tuple_list->attribute_list == nullptr) {
                        cout << c.find_by_parent_id(r->tuple_list->name);
                    } else if (::strcmp(r->tuple_list->name, "*") == 0 && r->tuple_list->next != nullptr) {
                        cout << c.find_by_field(r->tuple_list->next->attribute_list->field,
                                                r->tuple_list->next->attribute_list->value);
                    }
                } else if (r->operation == '=') {
                    Json::Value paramsList(Json::arrayValue);
                    paramsList.append(r->tuple_list->next->attribute_list->field);
                    paramsList.append(r->tuple_list->next->attribute_list->value);

                    struct attribute* a = r->tuple_list->next->attribute_list->composite_attribute;
                    while (a != nullptr) {
                        paramsList.append(a->field);
                        paramsList.append(a->value);
                        a = a->composite_attribute;
                    }

                    cout << c.update(paramsList, r->tuple_list->name);
                    free(request_str);
                }
            }
        } catch (JsonRpcException &e) {
            cerr << e.what() << endl;
        }
    }
}