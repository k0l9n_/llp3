#pragma once

#define MAX_STRING_SIZE 64

#include <stdbool.h>

enum attribute_condition {
	MORE = '>',
	LESS = '<',
	EQUAL = '=',
	NEG = '!',
	GOQ = '1',
	LOQ = '0'
};

enum bool_condition {
	AND = '&',
	OR = '|',
	NOT = '!'
};

struct attribute {
	char* field;
	char* value;
	enum attribute_condition condition;
	enum bool_condition bool_condition;
	struct attribute* composite_attribute;
	struct attribute* next;
};

struct attr_tuple {
	char* name;
	struct attribute* attribute_list;
	struct attr_tuple* next;
};

struct request {
	char operation;
	struct attr_tuple* tuple_list;
};
