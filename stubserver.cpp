#include "gen/abstractstubserver.h"
#include "src/filetools/big_data_tools.h"
#include "src/interface/basic_crud.h"
#include <cstring>
#include "src/ui/commands/tools/string_tools.h"
#include "src/ui/commands/find_by_command.h"
#include "src/ui/commands/update_command.h"
#include <jsonrpccpp/server/connectors/httpserver.h>
#include <iostream>

using namespace jsonrpc;
using namespace std;

class MyStubServer : public AbstractStubServer {
public:
    MyStubServer(AbstractServerConnector &connector, serverVersion_t type) : AbstractStubServer(connector, type) {};
    virtual std::string add(const Json::Value& keysValues, const std::string& parentId);
    virtual std::string find_by_field(const std::string& fieldName, const std::string& fieldValue);
    virtual std::string find_by_id(const std::string& nodeId);
    virtual std::string find_by_parent_id(const std::string& parentId);
    virtual std::string update(const Json::Value& keysValues, const std::string& nodeId);
    virtual std::string remove(const std::string& nodeId);
    virtual std::string print();
    virtual void init_file(FILE *file);

    FILE *database_file;
};

string MyStubServer::add(const Json::Value& keysValues, const std::string& parentId) {
    size_t pattern_size;
    struct tree_header *header = (struct tree_header*) malloc_test(sizeof(struct tree_header));

    read_tree_header(header, this->database_file);
    pattern_size = header->subheader->pattern_size;
    uint32_t *pattern_types = (uint32_t*) malloc_test(sizeof(uint32_t) * pattern_size);
    char **pattern_names = (char**) malloc_test(sizeof(char *) * pattern_size);

    for (int i = 0; i < pattern_size; i++) {
        pattern_types[i] = header->pattern[i]->header->type;
        pattern_names[i] = header->pattern[i]->key_value;
    }

    char **arr;
    char *input_str = (char*) malloc(sizeof(char) * 1024);
    ::strcat(input_str, "add ");
    ::strcat(input_str, parentId.c_str());
    for (int i = 0 ; i < keysValues.size()/2 ; i++) {
        ::strcat(input_str, " ");
        ::strcat(input_str, keysValues[i].asCString());
        ::strcat(input_str, "=");
        ::strcat(input_str, keysValues[i + 1].asCString());
    }

    cout << "input : " << input_str << endl;

    split(input_str, ' ', &arr);

    size_t code = add_input_item(this->database_file, arr, pattern_size, pattern_types, pattern_names);
    if (code != 0) {
        return "Error code: " + to_string(code);
    }

    return "Ok";
}
string MyStubServer::find_by_field(const std::string& fieldName, const std::string& fieldValue) {
    size_t pattern_size;
    struct tree_header *header = (struct tree_header*) malloc_test(sizeof(struct tree_header));

    read_tree_header(header, this->database_file);
    pattern_size = header->subheader->pattern_size;
    uint32_t *pattern_types = (uint32_t*) malloc_test(sizeof(uint32_t) * pattern_size);
    char **pattern_names = (char**) malloc_test(sizeof(char *) * pattern_size);

    for (int i = 0; i < pattern_size; i++) {
        pattern_types[i] = header->pattern[i]->header->type;
        pattern_names[i] = header->pattern[i]->key_value;
    }

    size_t c;
    char **arr;
    char *input_str = (char*) malloc(128);
    ::strcat(input_str, "find_by field ");
    ::strcat(input_str, fieldName.c_str());
    ::strcat(input_str, " ");
    ::strcat(input_str, fieldValue.c_str());

    c = split(input_str, ' ', &arr);

    return find_by(this->database_file, arr, pattern_size, pattern_types, pattern_names, c);
}
string MyStubServer::find_by_id(const std::string& nodeId) {
    size_t pattern_size;
    struct tree_header *header = (struct tree_header*) malloc_test(sizeof(struct tree_header));

    read_tree_header(header, this->database_file);
    pattern_size = header->subheader->pattern_size;
    uint32_t *pattern_types = (uint32_t*) malloc_test(sizeof(uint32_t) * pattern_size);
    char **pattern_names = (char**) malloc_test(sizeof(char *) * pattern_size);

    for (int i = 0; i < pattern_size; i++) {
        pattern_types[i] = header->pattern[i]->header->type;
        pattern_names[i] = header->pattern[i]->key_value;
    }

    size_t c;
    char **arr;
    char *input_str = (char*) malloc(128);
    ::strcat(input_str, "find_by id ");
    ::strcat(input_str, nodeId.c_str());

    c = split(input_str, ' ', &arr);

    return find_by(this->database_file, arr, pattern_size, pattern_types, pattern_names, c);
}
string MyStubServer::find_by_parent_id(const std::string& parentId) {
    size_t pattern_size;
    struct tree_header *header = (struct tree_header*) malloc_test(sizeof(struct tree_header));

    read_tree_header(header, this->database_file);
    pattern_size = header->subheader->pattern_size;
    uint32_t *pattern_types = (uint32_t*) malloc_test(sizeof(uint32_t) * pattern_size);
    char **pattern_names = (char**) malloc_test(sizeof(char *) * pattern_size);

    for (int i = 0; i < pattern_size; i++) {
        pattern_types[i] = header->pattern[i]->header->type;
        pattern_names[i] = header->pattern[i]->key_value;
    }

    size_t c;
    char **arr;
    char *input_str = (char*) malloc(128);
    ::strcat(input_str, "find_by parent ");
    ::strcat(input_str, parentId.c_str());

    c = split(input_str, ' ', &arr);

    return find_by(this->database_file, arr, pattern_size, pattern_types, pattern_names, c);
}
string MyStubServer::update(const Json::Value& keysValues, const std::string& nodeId) {
    size_t pattern_size;
    struct tree_header *header = (struct tree_header*) malloc_test(sizeof(struct tree_header));

    read_tree_header(header, this->database_file);
    pattern_size = header->subheader->pattern_size;
    uint32_t *pattern_types = (uint32_t*) malloc_test(sizeof(uint32_t) * pattern_size);
    char **pattern_names = (char**) malloc_test(sizeof(char *) * pattern_size);

    for (int i = 0; i < pattern_size; i++) {
        pattern_types[i] = header->pattern[i]->header->type;
        pattern_names[i] = header->pattern[i]->key_value;
    }

    char **arr;
    char *input_str = (char*) malloc(sizeof(char) * 1024);
    ::strcat(input_str, "add ");
    ::strcat(input_str, nodeId.c_str());
    for (int i = 0 ; i < keysValues.size()/2 ; i++) {
        ::strcat(input_str, " ");
        ::strcat(input_str, keysValues[i].asCString());
        ::strcat(input_str, "=");
        ::strcat(input_str, keysValues[i + 1].asCString());
    }

    size_t c = split(input_str, ' ', &arr);

    size_t code = update_item(this->database_file, arr, pattern_size, pattern_types, pattern_names, c);
    if (code != 0) {
        return "Error code: " + to_string(code);
    }

    return "Ok";
}
string MyStubServer::remove(const std::string& nodeId) {
    if (remove_tuple(this->database_file, atoi(nodeId.c_str()), 0) != CRUD_INVALID) {
        return "Removed successfully";
    }
    return "Such id does not exist";
}
string MyStubServer::print() {
    return print_tuple_array_from_file(this->database_file);
}

void MyStubServer::init_file(FILE *file) {
    printf("Initializing pattern.\nInput the number of fields in pattern: ");
    char *count_str = (char*) malloc(INPUT_LINE_SIZE);
    scanf("%s", count_str);
    while (!isNumeric(count_str)) {
        printf("Not-numeric input, try again: ");
        scanf("%s", count_str);
    }
    size_t count = strtol(count_str, NULL, 10);
    char *str;
    char **str_array = (char**) malloc_test(count * sizeof(char *));
    char *type = (char*) malloc(INPUT_LINE_SIZE);
    uint32_t *types = (uint32_t*) malloc_test(count * sizeof(uint32_t));
    size_t *sizes = (size_t*) malloc_test(count * sizeof(size_t));
    size_t temp_size;
    for (size_t iter = 0; iter < count; iter++) {
        printf("--- Field %-3zu ---\n", iter);
        str = (char*) malloc_test(INPUT_LINE_SIZE);
        printf("Enter field name: ");
        scanf("%s", str);
        str_array[iter] = str;
        temp_size = strlen(str);
        sizes[iter] = temp_size + (!(temp_size % FILE_GRANULARITY) ? 1 : 0);
        printf("%d. Boolean\n", BOOLEAN_TYPE);
        printf("%d. Integer\n", INTEGER_TYPE);
        printf("%d. Float\n", FLOAT_TYPE);
        printf("%d. String\n", STRING_TYPE);
        printf("Choose field type: ");
        scanf("%s", type);
        while (strlen(type) != 1) {
            printf("Incorrect input, try again: ");
            scanf("%s", type);
        }
        types[iter] = strtol(type, NULL, 10);
    }


    init_empty_file(file, str_array, types, count, sizes);

    this->database_file = file;

    for (size_t iter = 0; iter < count; iter++) free_test(str_array[iter]);
    free_test(str_array);
    free_test(sizes);
    free_test(types);
    free_test(count_str);
    free_test(type);
}

int main(int argc, char **argv) {
    char *main_filename;
    FILE *file;
    char flag;

    int port = atoi(argv[3]);
    HttpServer httpserver(port);
    MyStubServer s(httpserver,
                   JSONRPC_SERVER_V2);

    if (argc < 3 || argc > 4) {
        cout << "Wrong number of args!\nUsage: ./server <flag> <filename> <port>\n";
        return 1;
    }
    main_filename = argv[2];
    flag = argv[1][1];

    switch (flag) {
        case 'o':
            open_exist_file(main_filename, &file);
            s.database_file = file;
            break;
        case 'n':
            open_empty_file(main_filename, &file);
            s.init_file(file);
            break;
        default:
            printf("Unknown flag: -%c", flag);
            return 1;
    }

    size_t pattern_size;
    struct tree_header *header = (struct tree_header*) malloc_test(sizeof(struct tree_header));

    read_tree_header(header, file);
    pattern_size = header->subheader->pattern_size;
    uint32_t *pattern_types = (uint32_t*) malloc_test(sizeof(uint32_t) * pattern_size);
    char **pattern_names = (char**) malloc_test(sizeof(char *) * pattern_size);

    for (int i = 0; i < pattern_size; i++) {
        pattern_types[i] = header->pattern[i]->header->type;
        pattern_names[i] = header->pattern[i]->key_value;
    }

    s.StartListening();
    cout << "press enter to stop the server" << endl;
    getchar();
    s.StopListening();
    close_file(s.database_file);
}