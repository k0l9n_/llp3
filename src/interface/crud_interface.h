#ifndef CRUD_INTERFACE_H
#define CRUD_INTERFACE_H
#include <inttypes.h>
struct result_list_tuple {
    struct result_list_tuple *prev;
    struct database_tuple *value;
    uint64_t id;
};

#include "basic_crud.h"

size_t add_tuple(FILE *file, uint64_t *fields, uint64_t parent_id);
void print_tree_header_from_file(FILE *file);
char *print_tuple_array_from_file(FILE *file);

#endif

