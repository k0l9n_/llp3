server: stubserver.cpp src/filetools/basic_file_manager.c src/filetools/big_data_tools.c \
                       	  	src/generator/empty_generator.c src/interface/basic_crud.c \
                       	  	src/interface/crud_interface.c \
                       	  	src/ui/commands/add_command.c src/ui/commands/find_by_command.c \
                       	  	src/ui/commands/help_command.c src/ui/commands/update_command.c \
                       	  	src/ui/commands/tools/string_tools.c
		g++ -std=c++14 stubserver.cpp src/filetools/basic_file_manager.c src/filetools/big_data_tools.c \
	  	src/generator/empty_generator.c src/interface/basic_crud.c \
	  	src/interface/crud_interface.c \
	  	src/ui/commands/add_command.c src/ui/commands/find_by_command.c \
	  	src/ui/commands/help_command.c src/ui/commands/update_command.c \
	  	src/ui/commands/tools/string_tools.c -ljsoncpp -lmicrohttpd -ljsonrpccpp-common -ljsonrpccpp-server -o server

client: stubclient.cpp
		g++ -std=c++14 stubclient.cpp parser/src/parser.c -ljsoncpp -lcurl -ljsonrpccpp-common -ljsonrpccpp-client -fpermissive -o client

stub: spec.json
		jsonrpcstub spec.json --cpp-server=AbstractStubServer --cpp-client=StubClient
		mkdir -p gen
		mv abstractstubserver.h gen
		mv stubclient.h gen
